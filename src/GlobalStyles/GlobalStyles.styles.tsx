import { StyleSheet } from 'react-native';

export const globalStyles = StyleSheet.create({
  button: {
    alignSelf: 'center',
    borderWidth: 2,
    borderColor: 'white',
    borderRadius: 5,
    marginTop: 10,
    width: 280,
    maxWidth: '100%',
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  activeButton: {
    backgroundColor: 'white',
  },
  activeButtonText: {
    color: 'black',
  },
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
