import React, { useEffect } from 'react';
import { View, Text } from 'react-native';
import { styles } from '../Board/Board.styles';
import { GAME_TYPE } from '../Utils/Const';
import { useSelector } from 'react-redux';

interface BigSquareElementInterface {
  squareElements: any[];
  top: number;
  left: number;
  keyCounter: number;
  quadrant: number;
  highlightedQuadrant: number;
}

export const BigSquareElement: React.FC<BigSquareElementInterface> = (
  props: BigSquareElementInterface,
) => {
  const gameTypeStarted = useSelector((state: any) => state.counter.gameTypeStarted);

  return (
    <View
      key={props.keyCounter}
      style={[
        styles.squareContainer,
        {
          width: gameTypeStarted === GAME_TYPE.ULTIMATE ? 100 : 300,
          height: gameTypeStarted === GAME_TYPE.ULTIMATE ? 100 : 300,
          position: gameTypeStarted === GAME_TYPE.ULTIMATE ? 'absolute' : 'relative',
          top: gameTypeStarted === GAME_TYPE.ULTIMATE ? props.top : 0,
          left: gameTypeStarted === GAME_TYPE.ULTIMATE ? props.left : 0,
          borderWidth: gameTypeStarted === GAME_TYPE.ULTIMATE ? 2 : 1,
        },
      ]}
    >
      {props.squareElements}
    </View>
  );
};
