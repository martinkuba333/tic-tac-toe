import { configureStore } from '@reduxjs/toolkit';
import counterReducer from './ReduxState';

export default configureStore({
  reducer: {
    counter: counterReducer,
  },
});
