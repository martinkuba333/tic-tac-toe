import { createSlice, configureStore } from '@reduxjs/toolkit';
import { GAME_TYPE } from '../Utils/Const';

const initialState = {
  value: 1,
  gameTypeStarted: GAME_TYPE.NONE,
  isPlaying: false,
};

const reduxGameStates = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    increment: (state) => {
      state.value += 1;
    },
    setGameTypeStarted: (state, action) => {
      state.gameTypeStarted = action.payload;
    },
    setIsPlaying: (state, action) => {
      state.isPlaying = action.payload;
    },
  },
});

export const { increment, setGameTypeStarted, setIsPlaying } = reduxGameStates.actions;
export default reduxGameStates.reducer;

export const store = configureStore({
  reducer: {
    counter: reduxGameStates.reducer,
  },
});
