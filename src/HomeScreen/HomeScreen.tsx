import React from 'react';
import { View, Text, Pressable } from 'react-native';
import { useDispatch } from 'react-redux';
import { globalStyles } from '../GlobalStyles/GlobalStyles.styles';
import { setGameTypeStarted } from '../ReduxStore/ReduxState';
import { GAME_TYPE } from '../Utils/Const';

interface HomeScreenInterface {
  navigation: any;
}

export const HomeScreen: React.FC<HomeScreenInterface> = (props: HomeScreenInterface) => {
  const dispatch = useDispatch();

  return (
    <View style={globalStyles.container}>
      <Pressable
        style={globalStyles.button}
        onPress={() => {
          props.navigation.navigate('navNormal');
          dispatch(setGameTypeStarted(GAME_TYPE.NORMAL));
        }}
      >
        <Text style={globalStyles.buttonText}>Normal Tic-Tac-Toe</Text>
      </Pressable>
      <Pressable
        style={globalStyles.button}
        onPress={() => {
          props.navigation.navigate('navUltimate');
          dispatch(setGameTypeStarted(GAME_TYPE.ULTIMATE));
        }}
      >
        <Text style={globalStyles.buttonText}>Ultimate Tic-Tac-Toe</Text>
      </Pressable>
      <Pressable
        style={globalStyles.button}
        onPress={() => {
          props.navigation.navigate('navTutorial');
          dispatch(setGameTypeStarted(GAME_TYPE.TUTORIAL));
        }}
      >
        <Text style={globalStyles.buttonText}>Tutorial</Text>
      </Pressable>
    </View>
  );
};
