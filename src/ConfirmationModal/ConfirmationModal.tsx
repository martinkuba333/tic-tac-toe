import React, { useState } from 'react';
import { View, Text, Button, Modal } from 'react-native';
import { GAME_TYPE } from '../Utils/Const';
import { styles } from './ConfirmationModal.styles';
import BlackButton from '../BlackButton/BlackButton';
import { useDispatch, useSelector } from 'react-redux';
import { setGameTypeStarted, setIsPlaying } from '../ReduxStore/ReduxState';

interface ConfirmationModalInterface {
  restartGame?: any;
  modalVisible: boolean;
  setModalVisible: (visible: boolean) => void;
}

export const ConfirmationModal: React.FC<ConfirmationModalInterface> = (
  props: ConfirmationModalInterface,
) => {
  const dispatch = useDispatch();
  const gameTypeStarted = useSelector((state: any) => state.counter.gameTypeStarted);

  const handlePress = () => {
    if (props.restartGame) {
      props.restartGame();
    } else if (gameTypeStarted) {
      dispatch(setGameTypeStarted(GAME_TYPE.NONE));
      dispatch(setIsPlaying(false));
    }

    props.setModalVisible(false);
  };

  const createModalContent = (): JSX.Element => {
    return (
      <View style={styles.modal}>
        <View style={styles.cover}></View>
        <View style={styles.modalBackground}>
          <Text style={styles.modalTitle}>Are you sure?</Text>
          <View style={styles.modalButton}>
            <BlackButton title={'Yes'} onPress={handlePress} />
          </View>
          <View style={styles.modalButton}>
            <BlackButton title={'Cancel'} onPress={() => props.setModalVisible(false)} />
          </View>
        </View>
      </View>
    );
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={props.modalVisible}
      onRequestClose={() => {
        props.setModalVisible(false);
      }}
    >
      {createModalContent()}
    </Modal>
  );
};
