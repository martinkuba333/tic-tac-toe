import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  cover: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  modal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalBackground: {
    backgroundColor: 'white',
    padding: 15,
    width: 160,
    borderRadius: 5,
  },
  modalTitle: {
    textAlign: 'center',
    paddingBottom: 10,
    textTransform: 'uppercase',
    fontWeight: 700,
    fontSize: 16,
  },
  modalButton: {
    marginVertical: 5,
  },
});
