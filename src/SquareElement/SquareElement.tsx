import React, { useMemo } from 'react';
import { Text, Pressable, View } from 'react-native';
import { styles } from '../Board/Board.styles';
import { CONST, GAME_TYPE } from '../Utils/Const';
import Icon from 'react-native-dynamic-vector-icons';
import { SquareInterface } from '../Board/Board';
import { useSelector } from 'react-redux';

interface SquareElementInterface {
  square: SquareInterface;
  keyCounter: number;
  processPress: (index: number) => void;
  quadrant: number;
  highlightedQuadrant: number;
}

export const SquareElement: React.FC<SquareElementInterface> = (props: SquareElementInterface) => {
  const gameTypeStarted = useSelector((state: any) => state.counter.gameTypeStarted);

  const processIcon = () => {
    return (
      <View>
        {props.square.icon.name && props.square.icon.type && props.square.icon.color ? (
          <Icon
            key={props.keyCounter}
            name={props.square.icon.name}
            type={props.square.icon.type}
            color={props.square.icon.color}
            size={props.square.icon.size}
          />
        ) : (
          <Text></Text>
        )}
      </View>
    );
  };

  const squareElement = useMemo(() => {
    return (
      <Pressable
        key={props.keyCounter}
        style={[
          styles.square,
          {
            backgroundColor:
              gameTypeStarted === GAME_TYPE.ULTIMATE
                ? props.square.background === CONST.HIGHLIGHTED_SQUARE
                  ? CONST.HIGHLIGHTED_SQUARE
                  : props.highlightedQuadrant === props.quadrant
                    ? CONST.HIGHLIGHTED_QUADRANT
                    : props.highlightedQuadrant === CONST.ALL_QUADRANTS
                      ? CONST.HIGHLIGHTED_QUADRANT
                      : props.square.background
                : props.square.background,
            borderWidth: gameTypeStarted === GAME_TYPE.ULTIMATE ? 1 : 3,
            borderColor: gameTypeStarted === GAME_TYPE.ULTIMATE ? '#000' : '#000',
          },
        ]}
        onPress={() => props.processPress(props.square.index)}
      >
        {processIcon()}
      </Pressable>
    );
  }, [
    props.keyCounter,
    gameTypeStarted,
    props.square,
    props.quadrant,
    props.highlightedQuadrant,
    props.processPress,
  ]);

  return squareElement;
};
