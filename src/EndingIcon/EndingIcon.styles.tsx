import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  endingIcon: {
    position: 'absolute',
    zIndex: 10,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
});
