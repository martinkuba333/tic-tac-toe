import React, { useEffect, useState, useRef } from 'react';
import Icon, { IconType } from 'react-native-dynamic-vector-icons';
import { Animated, View } from 'react-native';
import { styles } from './EndingIcon.styles';
import { CONST, GAME_TYPE } from '../Utils/Const';
import { useSelector } from 'react-redux';

interface WinnerInterface {
  winnerPlayer: number;
  winnerQuadrant: number;
  ultimateEnding?: boolean;
}

const normalTop = 72;
const normalLeft = 300;
const ultimateLeft = 100;
const smallIconSize = 95;
const bigIconSize = 293;

export const EndingIcon: React.FC<WinnerInterface> = (props: WinnerInterface) => {
  const [left, setLeft] = useState<number>(-normalLeft);
  const [stopLeft, setStopLeft] = useState<number>(0);
  const [top, setTop] = useState<number>(normalTop);
  const [width, setWidth] = useState<number | null>(null);
  const gameTypeStarted = useSelector((state: any) => state.counter.gameTypeStarted);

  const opacity = useRef(new Animated.Value(0)).current; // Initial opacity value is 0

  useEffect(() => {
    if (props.winnerPlayer && left < stopLeft) {
      setWidth(gameTypeStarted === GAME_TYPE.ULTIMATE ? ultimateLeft : normalLeft);

      if (gameTypeStarted === GAME_TYPE.ULTIMATE && top === normalTop) {
        const newTop = normalTop + Math.floor(props.winnerQuadrant / 3) * ultimateLeft;
        setTop(newTop);
      }

      if (gameTypeStarted === GAME_TYPE.ULTIMATE && stopLeft === 0) {
        let newStopLeft = 0;
        if ([1, 4, 7].includes(props.winnerQuadrant)) {
          newStopLeft = ultimateLeft;
        } else if ([2, 5, 8].includes(props.winnerQuadrant)) {
          newStopLeft = ultimateLeft * 2;
        }

        setStopLeft(newStopLeft);
        setLeft(newStopLeft);
      } else {
        setLeft(0);
      }

      // Start opacity animation
      Animated.timing(opacity, {
        toValue: 0.8,
        duration: 300,
        useNativeDriver: true,
      }).start();
    }

    if (!props.winnerPlayer && left === stopLeft) {
      setLeft(-normalLeft);
      setTop(normalTop);
      setStopLeft(0);

      // Reset opacity to 0
      Animated.timing(opacity, {
        toValue: 0,
        duration: 300,
        useNativeDriver: true,
      }).start();
    }

    if (props.ultimateEnding) {
      setWidth(normalLeft);
    }
  }, [props.winnerPlayer, left, props.ultimateEnding]);

  const processWinningIcon = (): JSX.Element => {
    let icon = null;

    switch (props.winnerPlayer) {
      case CONST.PL1:
        icon = (
          <Animated.View
            style={[
              styles.endingIcon,
              {
                left,
                top,
                width,
                opacity, // Use animated opacity
                paddingLeft:
                  gameTypeStarted === GAME_TYPE.ULTIMATE ? (props.ultimateEnding ? 49 : 19) : 52,
                zIndex: props.ultimateEnding ? 10 : 1,
              },
            ]}
          >
            <Icon
              name={'times'}
              type={IconType.FontAwesome5}
              color={'green'}
              size={
                props.ultimateEnding
                  ? bigIconSize
                  : gameTypeStarted === GAME_TYPE.ULTIMATE
                    ? smallIconSize
                    : bigIconSize
              }
            />
          </Animated.View>
        );
        break;
      case CONST.PL2:
        icon = (
          <Animated.View
            style={[
              styles.endingIcon,
              {
                left,
                top,
                width,
                opacity, // Use animated opacity
                paddingLeft:
                  gameTypeStarted === GAME_TYPE.ULTIMATE ? (props.ultimateEnding ? 22 : 10) : 27,
                zIndex: props.ultimateEnding ? 10 : 1,
              },
            ]}
          >
            <Icon
              name={'circle-o'}
              type={IconType.FontAwesome}
              color={'red'}
              size={
                props.ultimateEnding
                  ? bigIconSize
                  : gameTypeStarted === GAME_TYPE.ULTIMATE
                    ? smallIconSize
                    : bigIconSize
              }
            />
          </Animated.View>
        );
        break;
      case CONST.NO_PL:
        icon = (
          <Animated.View
            style={[
              styles.endingIcon,
              {
                left,
                top,
                width,
                opacity,
                paddingLeft:
                  gameTypeStarted === GAME_TYPE.ULTIMATE ? (props.ultimateEnding ? 24 : 10) : 27,
                zIndex: props.ultimateEnding ? 10 : 1,
              },
            ]}
          >
            <Icon
              name={'equals'}
              type={IconType.FontAwesome5}
              color={'orange'}
              size={
                props.ultimateEnding
                  ? bigIconSize
                  : gameTypeStarted === GAME_TYPE.ULTIMATE
                    ? smallIconSize
                    : bigIconSize
              }
            />
          </Animated.View>
        );
        break;
      default:
        break;
    }
    return <>{icon}</>;
  };

  return <>{processWinningIcon()}</>;
};
