export const CONST = {
  ALL_QUADRANTS: 9,
  HIGHLIGHTED_QUADRANT: 'aqua',
  HIGHLIGHTED_SQUARE: 'yellow',
  PL1: 1,
  PL2: 2,
  NO_PL: 3,
};

export const GAME_TYPE = {
  NONE: 0,
  NORMAL: 1,
  ULTIMATE: 2,
  TUTORIAL: 3,
  SETTINGS: 4,
};

export enum Difficulty {
  EASY = 'EASY',
  HARD = 'HARD (in progress)',
}

export const WINNING_COMBINATIONS = [
  // Rows
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],

  // Columns
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],

  // Diagonals
  [0, 4, 8],
  [2, 4, 6],
];

//todo ideas
//different languages
//change icons
//change colors
//ai - hard- minimax algorithm
//tests
//counting wins for the player in normal tic-tac-toe - statistics?
//configurable background
//configurable background colors
//ultimate ultimate?? and on web??
//sound on every tap
//disable/enable restart+revert in settings?
//ai is set always on player2 - change to any
//online multiplayer + chit-chat?
//custom sized 3x3 4x4 5x5....?
//add achievements as gamification
//add hints and strategies
//replay whole game as video
//tutorial with images
//save the game for further checking?
//show/hide indexes of squares and add voice message?
//zoom in zoom out mode
//send feedback mail
//custom themes

//todo bugs
//play with ultimate ai can choose already winned quadrant
