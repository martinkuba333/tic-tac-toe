import React from 'react';
import { Pressable, Text } from 'react-native';
import { globalStyles } from '../GlobalStyles/GlobalStyles.styles';

interface RevertButtonInterface {
  turnAround: boolean;
  revertMove: any;
  isSinglePlayer?: boolean;
  disable: boolean;
}

export const RevertButton: React.FC<RevertButtonInterface> = (props: RevertButtonInterface) => {
  return (
    <Pressable
      style={[
        globalStyles.button,
        {
          transform: [{ rotate: props.turnAround ? '180deg' : '0deg' }],
          opacity: props.isSinglePlayer && props.turnAround ? 0 : 1,
        },
      ]}
      onPress={() => {
        props.revertMove();
      }}
    >
      <Text style={[globalStyles.buttonText, { opacity: props.disable ? 0.2 : 1 }]}>
        REVERT LAST MOVE
      </Text>
    </Pressable>
  );
};
