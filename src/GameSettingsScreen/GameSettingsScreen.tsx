import React, { useEffect } from 'react';
import { useNavigation } from '@react-navigation/native';
import { GameSettings } from './GameSettings';

function GameSettingsScreen({ navigation }: any): JSX.Element {
  const nav = useNavigation();

  useEffect(() => {
    const unsubscribe = nav.addListener('beforeRemove', () => {
      console.log('Back arrow pressed from GameSettings to Main');
    });

    return unsubscribe;
  }, [nav]);

  return <GameSettings />;
}

export { GameSettingsScreen };
