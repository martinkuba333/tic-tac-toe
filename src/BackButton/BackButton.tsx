import React, { useState } from 'react';
import { Pressable, View } from 'react-native';
import { globalStyles } from '../GlobalStyles/GlobalStyles.styles';
import Icon, { IconType } from 'react-native-dynamic-vector-icons';
import { ConfirmationModal } from '../ConfirmationModal/ConfirmationModal';
import { GAME_TYPE } from '../Utils/Const';
import { useDispatch, useSelector } from 'react-redux';
import { setGameTypeStarted, setIsPlaying } from '../ReduxStore/ReduxState';
import { styles } from './BackButton.styles';

interface RootState {
  counter: {
    isPlaying: boolean;
    gameTypeStarted: number;
  };
}

export const BackButton: React.FC = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const dispatch = useDispatch();
  const isPlaying = useSelector((state: RootState) => state.counter.isPlaying);
  const gameTypeStarted = useSelector((state: RootState) => state.counter.gameTypeStarted);

  const createBackButton = (): JSX.Element => {
    const top = gameTypeStarted === GAME_TYPE.TUTORIAL ? 825 : -60;

    return (
      <View style={styles.backButtonContainer}>
        <Pressable style={[globalStyles.button, styles.backButton, { top }]} onPress={handleGoBack}>
          <Icon name={'home'} type={IconType.FontAwesome} color={'white'} size={20} />
        </Pressable>
      </View>
    );
  };

  const handleGoBack = () => {
    if (isPlaying) {
      setModalVisible(true);
    } else {
      dispatch(setGameTypeStarted(GAME_TYPE.NONE));
      dispatch(setIsPlaying(false));
    }
  };

  return (
    <View
      style={
        gameTypeStarted === GAME_TYPE.SETTINGS || gameTypeStarted === GAME_TYPE.TUTORIAL
          ? { marginLeft: 20 }
          : null
      }
    >
      {gameTypeStarted === GAME_TYPE.NONE ? null : createBackButton()}
      <ConfirmationModal modalVisible={modalVisible} setModalVisible={setModalVisible} />
    </View>
  );
};
