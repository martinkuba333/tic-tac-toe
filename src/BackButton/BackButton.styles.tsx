import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  backButtonContainer: {
    position: 'relative',
  },
  backButton: {
    padding: 10,
    position: 'absolute',
    width: 'auto',
  },
});
