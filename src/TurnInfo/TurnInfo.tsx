import React, { useEffect } from 'react';
import { Text, View } from 'react-native';
import { styles } from './TurnInfo.styles';
import { CONST } from '../Utils/Const';
import Icon, { IconType } from 'react-native-dynamic-vector-icons';

interface TurnInfoInterface {
  turnAround: boolean;
  player: number;
  isSinglePlayer?: boolean;
}

export const TurnInfo: React.FC<TurnInfoInterface> = (props: TurnInfoInterface) => {
  return (
    <View>
      <Text
        style={[
          styles.playerTurnInfo,
          {
            transform: [{ rotate: props.turnAround ? '180deg' : '0deg' }],
            opacity: props.isSinglePlayer && props.turnAround ? 0 : 1,
          },
        ]}
      >
        Player turn:<Text> {/* just one empty space */}</Text>
        {props.player === CONST.PL1 ? (
          <Icon name={'times'} type={IconType.FontAwesome5} color={'green'} size={30} />
        ) : null}
        {props.player === CONST.PL2 ? (
          <Icon name={'circle-o'} type={IconType.FontAwesome} color={'red'} size={25} />
        ) : null}
      </Text>
    </View>
  );
};
