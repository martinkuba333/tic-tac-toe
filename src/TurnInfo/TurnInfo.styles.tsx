import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  playerTurnInfo: {
    textTransform: 'uppercase',
    color: 'white',
    fontSize: 33,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 15,
    marginBottom: 15,
  },
});
