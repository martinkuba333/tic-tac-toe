import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  cubeContainer: {
    overflow: 'hidden',
  },
  boardContainer: {
    position: 'relative',
    width: 300,
    height: 300,
  },
  squareContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    borderColor: '#000',
  },
  square: {
    width: '33.33%',
    height: '33.33%',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
});
