import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { IconType } from 'react-native-dynamic-vector-icons';
import { CONST, Difficulty, GAME_TYPE, WINNING_COMBINATIONS } from '../Utils/Const';
import { styles } from './Board.styles';
import { TurnInfo } from '../TurnInfo/TurnInfo';
import { RevertButton } from '../RevertButton/RevertButton';
import { EndingIcon } from '../EndingIcon/EndingIcon';
import { RestartButton } from '../RestartButton/RestartButton';
import { SquareElement } from '../SquareElement/SquareElement';
import { BigSquareElement } from '../BigSquareElement/BigSquareElement';
import { useDispatch, useSelector } from 'react-redux';
import { setIsPlaying } from '../ReduxStore/ReduxState';

interface IconInterface {
  name: string | null;
  type: IconType | null;
  color: string | null;
  player: number | null;
  size: number;
}

export interface SquareInterface {
  icon: IconInterface;
  index: number;
  background: string;
  quadrant: number;
}

interface GameSettingsInterface {
  isSinglePlayer: boolean;
  difficulty: Difficulty;
}

interface PastMoves {
  quadrant: number;
  index: number;
  player: number;
}

interface WinnerInterface {
  winnerPlayer: number;
  winnerQuadrant: number;
}

const FIRST_TURN = 1;

export const Board: React.FC<GameSettingsInterface> = (props: GameSettingsInterface) => {
  const [player, setPlayer] = useState<number>(CONST.PL1);
  const [squares, setSquares] = useState<SquareInterface[]>([]);
  const [turn, setTurn] = useState<number>(FIRST_TURN);
  const [winner, setWinner] = useState<WinnerInterface[]>([]);
  const [moves, setMoves] = useState<PastMoves[]>([]);
  const [moveReverted, setMoveReverted] = useState<boolean>(false);
  const [disableRevert, setDisableRevert] = useState<boolean>(false);
  const [ultimateWinner, setUltimateWinner] = useState<number | null>(null);
  const [highlightedQuadrant, setHighlightedQuadrant] = useState<number>(CONST.ALL_QUADRANTS);
  const gameTypeStarted = useSelector((state: any) => state.counter.gameTypeStarted);
  const dispatch = useDispatch();

  useEffect(() => {
    fillSquaresWithEmptyValues();
    dispatch(setIsPlaying(true));
  }, []);

  useEffect(() => {
    if (props.isSinglePlayer && player === CONST.PL2 && !moveReverted) {
      setTimeout(() => {
        processAiMovement();
      }, 300);
    }
  }, [player]);

  const fillSquaresWithEmptyValues = () => {
    let squares: SquareInterface[] = [];
    let indexCounter = 0;
    const size = gameTypeStarted === GAME_TYPE.ULTIMATE ? 23 : 70;
    for (let j = 0; j < (gameTypeStarted === GAME_TYPE.ULTIMATE ? 9 : 1); j++) {
      for (let i = 0; i < 9; i++) {
        squares.push({
          icon: {
            name: null,
            type: null,
            color: null,
            player: null,
            size: size,
          },
          index: indexCounter,
          background: '#ffffff',
          quadrant: j,
        });
        indexCounter++;
      }
    }

    setSquares(squares);
  };

  const createSquares = (): JSX.Element[] => {
    const bigSquareElements: JSX.Element[] = [];
    const jSize = gameTypeStarted === GAME_TYPE.ULTIMATE ? 9 : 1;
    let keyCounter = 0;
    let top = -100;
    let left = 0;

    for (let j = 0; j < jSize; j++) {
      top = j % 3 === 0 ? top + 100 : top;
      left = j % 3 === 0 ? 0 : left + 100;
      let squareElements = [];

      for (let i = 0; i < 9; i++) {
        const square = squares[keyCounter];
        squareElements.push(
          <SquareElement
            key={keyCounter}
            square={square}
            keyCounter={keyCounter}
            processPress={processPress}
            quadrant={j}
            highlightedQuadrant={highlightedQuadrant}
          />,
        );
        keyCounter++;
      }

      bigSquareElements.push(
        <BigSquareElement
          key={keyCounter}
          squareElements={squareElements}
          top={top}
          left={left}
          keyCounter={keyCounter}
          quadrant={j}
          highlightedQuadrant={highlightedQuadrant}
        />,
      );
    }

    return bigSquareElements;
  };

  const changePlayerTurn = () => {
    setPlayer((prevState): number => {
      if (prevState !== CONST.NO_PL) {
        return prevState === CONST.PL1 ? CONST.PL2 : CONST.PL1;
      }
      return prevState;
    });
  };

  const processPress = (index: number): void => {
    if (squares[index].icon.name !== null) {
      return;
    }

    if (
      gameTypeStarted === GAME_TYPE.ULTIMATE &&
      highlightedQuadrant !== CONST.ALL_QUADRANTS &&
      squares[index].quadrant !== highlightedQuadrant
    ) {
      return;
    }

    if ((props.isSinglePlayer && player === CONST.PL1) || !props.isSinglePlayer) {
      createMark(index);
      setMoves((prevState) => [
        ...prevState,
        { quadrant: squares[index].quadrant, index: index, player },
      ]);
      setMoveReverted(false);
    }

    checkQuadrantEndGame(index);
    if (gameTypeStarted === GAME_TYPE.ULTIMATE) {
      checkUltimateEndGame();
      if (!ultimateWinner) {
        let newHighlightedQuadrant = index % 9;
        const isQuadrantOccupied = winner.filter(
          (w) => w.winnerQuadrant === newHighlightedQuadrant,
        );
        if (isQuadrantOccupied.length) {
          newHighlightedQuadrant = CONST.ALL_QUADRANTS;
        }
        setHighlightedQuadrant(newHighlightedQuadrant);
      }
    }

    if ((props.isSinglePlayer && player === CONST.PL1) || !props.isSinglePlayer) {
      setTurn((prevState) => prevState + 1);
      changePlayerTurn();
    }
  };

  const checkQuadrantEndGame = (index: number): void => {
    const quadrant = squares[index].quadrant;

    if (turn >= 5) {
      if (gameTypeStarted === GAME_TYPE.ULTIMATE && isGameDraw(quadrant)) {
        const newWinners = winner;
        newWinners.push({ winnerPlayer: CONST.NO_PL, winnerQuadrant: quadrant });
        setWinner(newWinners);
        return;
      }
      const winningPlayer = getWinnerPlayer(quadrant);
      if (winningPlayer) {
        if (gameTypeStarted === GAME_TYPE.ULTIMATE) {
          const newWinners = winner;
          newWinners.push({ winnerPlayer: player, winnerQuadrant: quadrant });
          setWinner(newWinners);
        } else {
          setWinner([{ winnerPlayer: player, winnerQuadrant: quadrant }]);
          if (props.isSinglePlayer) setDisableRevert(true);
          setPlayer(CONST.NO_PL);
        }
        return;
      }
      if (gameTypeStarted === GAME_TYPE.NORMAL && isGameDraw(quadrant)) {
        setWinner([{ winnerPlayer: CONST.NO_PL, winnerQuadrant: quadrant }]);
        if (props.isSinglePlayer) setDisableRevert(true);
        setPlayer(CONST.NO_PL);
      }
    }
  };

  const checkUltimateEndGame = (): void => {
    const playerWins: { [player: number]: number[] } = {};

    // Count the number of wins for each player in each quadrant
    for (const { winnerPlayer, winnerQuadrant } of winner) {
      if (!playerWins[winnerPlayer]) {
        playerWins[winnerPlayer] = [];
      }
      playerWins[winnerPlayer].push(winnerQuadrant);
    }

    // Check if any player has won three adjacent quadrants
    for (const player in playerWins) {
      const playerQuadrants = playerWins[player];

      for (const combination of WINNING_COMBINATIONS) {
        const isWinner = combination.every((quad) => playerQuadrants.includes(quad));
        if (isWinner) {
          setUltimateWinner(Number(player));
          return;
        }
      }
    }
    //Check Draw
    if (winner.length === 9) {
      setUltimateWinner(CONST.NO_PL);
      return;
    }
  };

  const createMark = (index: number) => {
    let newSquares = [...squares];
    if (newSquares[index].icon.player === null) {
      if (player === CONST.PL1) {
        newSquares[index].icon = {
          name: 'times',
          type: IconType.FontAwesome5,
          color: 'green',
          player: CONST.PL1,
          size: newSquares[index].icon.size,
        };
      }
      if (player === CONST.PL2) {
        newSquares[index].icon = {
          name: 'circle-o',
          type: IconType.FontAwesome,
          color: 'red',
          player: CONST.PL2,
          size: newSquares[index].icon.size,
        };
      }
    }

    newSquares = highlightLastMove(newSquares, index);
    setSquares(newSquares);
  };

  const highlightLastMove = (newSquares: SquareInterface[], index: number): SquareInterface[] => {
    newSquares = clearHighlights(newSquares);
    newSquares[index].background = CONST.HIGHLIGHTED_SQUARE;
    return newSquares;
  };

  const clearHighlights = (newSquares: SquareInterface[]): SquareInterface[] => {
    for (const s of newSquares) {
      s.background = '#ffffff';
    }
    return newSquares;
  };

  const processAiMovement = () => {
    const emptyIndexes = getAllPossibleMovementsForAi();
    let aiChosenIndex = getAiMove(emptyIndexes);
    createMark(aiChosenIndex);
    setMoves((prevState) => [
      ...prevState,
      { quadrant: squares[aiChosenIndex].quadrant, index: aiChosenIndex, player },
    ]);
    checkQuadrantEndGame(aiChosenIndex);
    if (gameTypeStarted === GAME_TYPE.ULTIMATE) {
      checkUltimateEndGame();
      if (!ultimateWinner) {
        let newHighlightedQuadrant = aiChosenIndex % 9;
        const isQuadrantOccupied = winner.filter(
          (w) => w.winnerQuadrant === newHighlightedQuadrant,
        );
        if (isQuadrantOccupied.length) {
          newHighlightedQuadrant = CONST.ALL_QUADRANTS;
        }
        setHighlightedQuadrant(newHighlightedQuadrant);
      }
    }
    setTurn((prevState) => prevState + 1);
    changePlayerTurn();
  };

  const getAllPossibleMovementsForAi = (): number[] => {
    const emptyIndexes: number[] = [];
    let winnerQuadrants = winner.map((w) => w.winnerQuadrant);
    for (let i = 0; i < squares.length; i++) {
      let q = squares[i].quadrant;
      if (gameTypeStarted === GAME_TYPE.ULTIMATE) {
        if (
          (highlightedQuadrant === CONST.ALL_QUADRANTS || highlightedQuadrant === q) &&
          squares[i].icon.name === null
        ) {
          emptyIndexes.push(i);
        }
      } else if (squares[i].icon.name === null && winnerQuadrants.indexOf(q) === -1) {
        emptyIndexes.push(i);
      }
    }
    return emptyIndexes;
  };

  const getAiMove = (emptyIndexes: number[]): number => {
    let aiChosenIndex: number;
    if (props.difficulty === Difficulty.EASY) {
      const randomIndex = Math.floor(Math.random() * emptyIndexes.length);
      aiChosenIndex = emptyIndexes[randomIndex];
    } else {
      //same as EASY because HARD is missing for now
      const randomIndex = Math.floor(Math.random() * emptyIndexes.length);
      aiChosenIndex = emptyIndexes[randomIndex];
    }
    return aiChosenIndex;
  };

  const getWinnerPlayer = (quadrant: number): number | null => {
    for (const combination of WINNING_COMBINATIONS) {
      let [a, b, c] = combination;
      if (quadrant > 0) {
        a += quadrant * 9;
        b += quadrant * 9;
        c += quadrant * 9;
      }

      const player = squares[a].icon.player;
      if (
        player !== null &&
        player === squares[b].icon.player &&
        player === squares[c].icon.player
      ) {
        return player;
      }
    }
    return null;
  };

  const isGameDraw = (quadrant: number): boolean => {
    for (let i = quadrant * 9; i < quadrant * 9 + 9; i++) {
      if (squares[i].icon.player === null) {
        return false;
      }
    }
    return true;
  };

  const restartGame = () => {
    setPlayer(CONST.PL1);
    setTurn(FIRST_TURN);
    fillSquaresWithEmptyValues();
    setWinner([]);
    setUltimateWinner(null);
    setMoves([]);
    setDisableRevert(false);
    setHighlightedQuadrant(CONST.ALL_QUADRANTS);
  };

  const revertMove = (): void => {
    if (gameTypeStarted === GAME_TYPE.NORMAL && props.isSinglePlayer && winner.length) {
      return;
    }
    const newMoves: PastMoves[] = [...moves];
    const lastMove = newMoves.pop();

    if (lastMove) {
      processRevertMove(newMoves, lastMove);
    }
  };

  const processRevertMove = (newMoves: PastMoves[], lastMove: PastMoves) => {
    let newSquares = [...squares];
    newSquares = clearSquareIndex(newSquares, lastMove.index);

    if (winner.length && gameTypeStarted === GAME_TYPE.NORMAL) {
      processRevertOnEndGame(lastMove);
    } else {
      setPlayer(
        props.isSinglePlayer
          ? lastMove.player === CONST.PL1
            ? CONST.PL2
            : CONST.PL1
          : lastMove.player,
      );
      const penultimateMove = props.isSinglePlayer ? newMoves.pop() : null;
      if (penultimateMove && penultimateMove.index) {
        newSquares = clearSquareIndex(newSquares, penultimateMove.index);
      }
      setHighlightedQuadrant(lastMove.quadrant);
    }
    lastStepsInRevert(newSquares, newMoves);
  };

  const processRevertOnEndGame = (lastMove: PastMoves) => {
    const newWinner = winner;
    newWinner.pop();
    setWinner(newWinner);

    setPlayer(
      props.isSinglePlayer
        ? lastMove.player === CONST.PL1
          ? CONST.PL1
          : CONST.PL2
        : lastMove.player,
    );
  };

  const clearSquareIndex = (newSquares: SquareInterface[], index: number): SquareInterface[] => {
    newSquares[index].icon = {
      name: null,
      type: null,
      color: null,
      player: null,
      size: newSquares[index].icon.size,
    };
    return newSquares;
  };

  const lastStepsInRevert = (newSquares: SquareInterface[], newMoves: PastMoves[]) => {
    const decreaseTurn = props.isSinglePlayer ? 2 : 1;
    newSquares = clearHighlights(newSquares);
    if (newMoves.length) {
      newSquares = highlightLastMove(newSquares, newMoves[newMoves.length - 1].index);
    }
    setTurn((prevState) => prevState - decreaseTurn);
    setSquares(newSquares);
    setMoves(newMoves);
    setMoveReverted(true);
  };

  const createEndingIcons = (): JSX.Element[] => {
    const icons = [];
    for (let i = 0; i < winner.length; i++) {
      icons.push(
        <EndingIcon
          key={i}
          winnerPlayer={winner[i].winnerPlayer}
          winnerQuadrant={winner[i].winnerQuadrant}
        />,
      );
    }
    return icons;
  };

  const createUltimateEndingIcon = (): JSX.Element | null => {
    if (ultimateWinner) {
      return <EndingIcon winnerPlayer={ultimateWinner} winnerQuadrant={0} ultimateEnding={true} />;
    }
    return null;
  };

  return (
    <View style={styles.cubeContainer}>
      {createEndingIcons()}
      {createUltimateEndingIcon()}
      <TurnInfo turnAround={true} player={player} isSinglePlayer={props.isSinglePlayer} />
      <View style={styles.boardContainer}>{squares.length ? createSquares() : null}</View>
      <TurnInfo turnAround={false} player={player} />
      <RevertButton turnAround={false} revertMove={revertMove} disable={disableRevert} />
      <RestartButton restartGame={restartGame} />
    </View>
  );
};
