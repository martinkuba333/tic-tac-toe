import React, { useState } from 'react';
import { Pressable, Text, View } from 'react-native';
import { globalStyles } from '../GlobalStyles/GlobalStyles.styles';
import { Board } from '../Board/Board';
import { Difficulty, GAME_TYPE } from '../Utils/Const';

enum PlayerOption {
  SINGLE_PLAYER = 'Single Player',
  MULTI_PLAYER = 'Multi Player',
}

enum ButtonText {
  START_GAME = 'Start Game',
  CHOOSE_DIFICULTY = 'Difficulty',
  CHOOSE_PLAYER = 'Choose Player',
  CHOOSE_COLOR = 'Choose Color',
  CHOOSE_ICON = 'Choose Icon',
}

export const GameSettings: React.FC = () => {
  const [gameStarted, setGameStarted] = useState<boolean>(false);
  const [isSinglePlayer, setIsSinglePlayer] = useState<boolean>(true);
  const [difficulty, setDifficulty] = useState<Difficulty>(Difficulty.EASY);

  const createButton = (text: string, onPress: () => void): JSX.Element => {
    return (
      <Pressable
        style={[
          globalStyles.button,
          (text === PlayerOption.SINGLE_PLAYER && isSinglePlayer) ||
          (text === PlayerOption.MULTI_PLAYER && !isSinglePlayer)
            ? globalStyles.activeButton
            : {},
        ]}
        onPress={onPress}
      >
        <Text
          style={[
            globalStyles.buttonText,
            (text === PlayerOption.SINGLE_PLAYER && isSinglePlayer) ||
            (text === PlayerOption.MULTI_PLAYER && !isSinglePlayer)
              ? globalStyles.activeButtonText
              : {},
          ]}
        >
          {text}
        </Text>
      </Pressable>
    );
  };

  const createOptions = (): JSX.Element => {
    return (
      <View>
        <View style={[{ width: '40%' }]}>
          {createButton(PlayerOption.SINGLE_PLAYER, () => {
            setIsSinglePlayer(true);
          })}
        </View>
        <View style={[{ width: '40%', position: 'absolute', right: 0 }]}>
          {createButton(PlayerOption.MULTI_PLAYER, () => {
            setIsSinglePlayer(false);
          })}
        </View>
        <View style={isSinglePlayer ? null : { opacity: 0, pointerEvents: 'none' }}>
          {createButton(ButtonText.CHOOSE_DIFICULTY + '-' + difficulty, () => {
            const newDiff = difficulty === Difficulty.EASY ? Difficulty.HARD : Difficulty.EASY;
            setDifficulty(newDiff);
          })}
        </View>
        {createButton(ButtonText.CHOOSE_PLAYER, () => {})}
        {createButton(ButtonText.CHOOSE_COLOR, () => {})}
        {createButton(ButtonText.CHOOSE_ICON, () => {})}
        {createButton(ButtonText.START_GAME, () => {
          setGameStarted(true);
        })}
      </View>
    );
  };

  return (
    <View>
      {gameStarted ? (
        <Board isSinglePlayer={isSinglePlayer} difficulty={difficulty} />
      ) : (
        <View>{createOptions()}</View>
      )}
    </View>
  );
};
