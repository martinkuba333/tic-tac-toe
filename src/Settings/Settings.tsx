import React from 'react';
import { View, Text } from 'react-native';
import { BackButton } from '../BackButton/BackButton';
import { styles } from './Settings.styles';

export const Settings: React.FC = () => {
  return (
    <View style={styles.settingsContainer}>
      <View>
        <Text style={styles.title}>Settings</Text>
        <Text style={styles.text}>This is the perfect place for some settings.</Text>
      </View>
    </View>
  );
};
