export const TutorialTexts = {
  title: {
    title: 'Ultimate Tic-Tac-Toe Tutorial',
    text: `Welcome to the Ultimate Tic-Tac-Toe tutorial! In this tutorial, we'll learn how to play Ultimate Tic-Tac-Toe, a strategic variation of the classic game.`,
  },
  objective: {
    title: 'Objective',
    text: `The objective of Ultimate Tic-Tac-Toe is the same as regular Tic-Tac-Toe: to get three of your symbols in a row, column, or diagonal. However, in Ultimate Tic-Tac-Toe, the game is played on a larger board consisting of nine smaller Tic-Tac-Toe boards.`,
  },
  setup: {
    title: 'Game Setup',
    text: `Ultimate Tic-Tac-Toe is played on a 3x3 grid, and each cell of this grid contains another 3x3 grid. Players take turns placing their symbol (X or O) in one of the empty cells. The game starts with an empty board.`,
  },
  rules: {
    title: 'Game Rules',
    text: `The game begins with Player X making the first move. The cell in which a player places their symbol determines the next board their opponent must play in. For example, if Player X places their symbol in the bottom-right cell of a board, Player O must make their move in the bottom-right board. If a player wins a smaller Tic-Tac-Toe board, they claim that cell on the larger Ultimate Tic-Tac-Toe board. This cell then becomes their symbol. To win the game, a player must win three small Tic-Tac-Toe boards in a row, column, or diagonal, just like in regular Tic-Tac-Toe. If a player is sent to a board that has already been won or is full, they may choose any empty cell on the larger board.`,
  },
  tips: {
    title: 'Strategy Tips',
    text: `Focus on winning the smaller Tic-Tac-Toe boards first, as this will determine the outcome of the larger board. Pay attention to your opponent's moves and plan your moves accordingly to block them from winning smaller boards. Aim to create multiple winning opportunities across the larger board to increase your chances of winning. If you're forced to play in a board that has already been won, strategize to disrupt your opponent's winning pattern.`,
  },
  conclusion: {
    title: 'Conclusion',
    text: `Now that you know the rules and strategies of Ultimate Tic-Tac-Toe, it's time to put your skills to the test! Challenge your friends or play against the computer to see who can become the Ultimate Tic-Tac-Toe champion. Good luck and have fun!`,
  },
};
