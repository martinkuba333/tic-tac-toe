import React from 'react';
import { Text, View, ScrollView } from 'react-native';
import { styles } from './Tutorial.styles';
import { TutorialTexts } from './TutorialTexts';

type ContentTypes = {
  title: string;
  text: string;
};

export const Tutorial: React.FC = () => {
  const renderTextContent = (content: ContentTypes): JSX.Element => (
    <View key={content.title} style={styles.contentContainer}>
      <Text style={styles.title}>{content.title}</Text>
      <Text style={styles.text}>{content.text}</Text>
    </View>
  );

  return (
    <View style={styles.tutorialContainer}>
      <ScrollView contentContainerStyle={styles.scrollContent}>
        {Object.values(TutorialTexts).map((content: ContentTypes) => renderTextContent(content))}
      </ScrollView>
    </View>
  );
};
