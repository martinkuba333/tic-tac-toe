import React, { useState } from 'react';
import { Pressable, Text, View } from 'react-native';
import { globalStyles } from '../GlobalStyles/GlobalStyles.styles';
import { ConfirmationModal } from '../ConfirmationModal/ConfirmationModal';

interface RestartButtonInterface {
  restartGame: any;
}

export const RestartButton: React.FC<RestartButtonInterface> = (props: RestartButtonInterface) => {
  const [modalVisible, setModalVisible] = useState(false);

  const handleRestart = () => {
    setModalVisible(true);
  };

  return (
    <View>
      <Pressable
        style={[globalStyles.button]}
        onPress={() => {
          handleRestart();
        }}
      >
        <Text style={globalStyles.buttonText}>RESTART</Text>
      </Pressable>
      <ConfirmationModal
        restartGame={props.restartGame}
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
      />
    </View>
  );
};
