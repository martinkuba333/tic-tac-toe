import { View } from 'react-native';
import { Provider } from 'react-redux';
import store from './src/ReduxStore/Store';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { GameSettings } from './src/GameSettings/GameSettings';
import { Tutorial } from './src/Tutorial/Tutorial';
import { HomeScreen } from './src/HomeScreen/HomeScreen';
import { globalStyles } from './src/GlobalStyles/GlobalStyles.styles';
import Icon, { IconType } from 'react-native-dynamic-vector-icons';
import { BackButton } from './src/BackButton/BackButton';

function Home({ navigation }: any): JSX.Element {
  return <HomeScreen navigation={navigation} />;
}

function navNormal({ navigation }: any): JSX.Element {
  return (
    <View style={globalStyles.container}>
      <GameSettings />
    </View>
  );
}

function navUltimate({ navigation }: any): JSX.Element {
  return (
    <View style={globalStyles.container}>
      <GameSettings />
    </View>
  );
}

function navTutorial({ navigation }: any): JSX.Element {
  return (
    <View style={globalStyles.container}>
      <Tutorial />
    </View>
  );
}

const Stack = createNativeStackNavigator();

export default function App(): JSX.Element {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen
            name="Home"
            component={Home}
            options={{
              title: 'HOME',
              headerTitleStyle: {
                fontWeight: 'bold',
                fontSize: 20,
              },
            }}
          />
          <Stack.Screen
            name="navNormal"
            component={navNormal}
            options={{
              title: 'NORMAL TIC-TAC-TOE',
              headerTitleStyle: {
                fontWeight: 'bold',
                fontSize: 20,
              },
            }}
          />
          <Stack.Screen
            name="navUltimate"
            component={navUltimate}
            options={{
              title: 'ULTIMATE TIC-TAC-TOE',
              headerTitleStyle: {
                fontWeight: 'bold',
                fontSize: 20,
              },
            }}
          />
          <Stack.Screen
            name="navTutorial"
            component={navTutorial}
            options={{
              title: 'TUTORIAL',
              headerTitleStyle: {
                fontWeight: 'bold',
                fontSize: 24,
              },
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
